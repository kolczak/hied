package hied.beavers.predictors;

public interface IPredictor {
	void initialize ();
	Double predict (int userId, int movieId);
}
