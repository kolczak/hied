package hied.beavers.predictors;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class AveragePredictor implements IPredictor{
	HashMap<Integer, Double> usersAvgMarks;
	
	public void initialize () {
		usersAvgMarks = new HashMap<Integer, Double>();
		FileInputStream fstream = null;
		BufferedReader br;
		String strLine;
		
		try {
			fstream = new FileInputStream(new File("data/user_average_mark.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  // Get the object of DataInputStream
		DataInputStream in = new DataInputStream(fstream);
		br = new BufferedReader(new InputStreamReader(in));
		
		try {
			while ((strLine = br.readLine()) != null)   {
				String[] entry = strLine.split(";");
				if ( entry.length == 2 )//(entry[0].trim() != "") && (entry[1].trim() != "") ) 
				usersAvgMarks.put(
						Integer.parseInt(entry[0].trim()),
						Double.parseDouble(entry[1].trim()));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public Double predict(int userId, int movieId) {
		// TODO Auto-generated method stub
		return usersAvgMarks.get(userId);
	}

}
