/**
 * 
 */
package hied.beavers.general;

import java.io.File;

import hied.beavers.predictors.AveragePredictor;
import hied.beavers.predictors.IPredictor;

/**
 * @author kolczak
 *
 */
public class HiedPrediction {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		IPredictor predictor = new AveragePredictor();
		predictor.initialize();
		Worker.getInstance().predict(
				new File("data/qualifier.txt"), 
				new File("data/results.txt"),
				predictor);

	}

}
