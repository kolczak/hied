package hied.beavers.general;

import hied.beavers.predictors.IPredictor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class Worker {
	
	public static Worker Instance = null;
	
	public static Worker getInstance () {
		if (Instance == null) {
			Instance = new Worker();
		}
		return Instance;
	}
	
	private Worker () {} 

	public void predict(File inFile, File outFile, IPredictor predictor) {
		//manage input file
		FileInputStream fstream = null;
		BufferedWriter out = null;
		BufferedReader br;
		
		try {
			fstream = new FileInputStream(inFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  // Get the object of DataInputStream
		DataInputStream in = new DataInputStream(fstream);
		br = new BufferedReader(new InputStreamReader(in));
		
		//manage output file
		try {
		    out = new BufferedWriter(new FileWriter(outFile));
		} catch (IOException e) {
		}
		
		readPredictWrite(br, out, predictor);
		
		try {
			in.close();
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void readPredictWrite(BufferedReader in, BufferedWriter out, IPredictor predictor) {
		int movieId = -1;
		int userId = -1;
		boolean newMovie = false;
		String strLine;
		
		try {
			while ((strLine = in.readLine()) != null)   {
				  if (strLine.contains(":")) {
					  movieId = Integer.parseInt(strLine.split(":")[0]);
					  newMovie = true;
				  }
				  else {
					  userId = Integer.parseInt(strLine.split(",")[0]);
					  newMovie = false;
				  }
				  
				  if (newMovie)
					  out.write(movieId + ":\n");
				  else
					  out.write(userId + "," + predictor.predict(userId, movieId) + "\n");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} 
}
